#include "cell.h"

/**
 * @brief cell::cell konstruktor
 */
cell::cell(){
}

/**
 * @brief cell::~cell destrutkor
 */
cell::~cell(){}


/**
 * @brief cell::setParam - setter na nastavovani potrebnych parametru pro bunku
 * @param newState - pocatecni stav bunky
 */
void cell::setParam(double newState){

    this->state = newState;
}

/**
 * @brief cell::getStates getter ktery  vrati  stav bunku v urcity casovy okamzik t
 * @return  double hodnotu pole state objektu tridy cell
 */
double cell::getState() const{
    return this->state;
}

/**
 * @brief cell::setState setter ktery nastavuje potrebnou hodnotu pole state
 * @param newState stav bunky pro nastavovani
 */
void cell::setState(double newState){

    this->state = newState;
}


void cell::setType(std::string typeValue) {
    this->type = typeValue;
}

std::string cell::getType() const {
    return this->type;
}


void cell::setSlope(double slopeValue){
    this->sloupe = slopeValue;
}

double cell::getSlope() const{
    return this->sloupe;
}

void cell::setCoeff(double coeffValue){
    this->coeff = coeffValue;

}

double cell::getCoeff() const{
    return this->coeff;
}
