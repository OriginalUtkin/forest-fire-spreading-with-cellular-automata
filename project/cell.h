#ifndef CELL_H
#define CELL_H
#include <string>
#include <iomanip>
#include <limits>



class cell
{
public:
    cell();
    ~cell();
    std::string stringRep();

    //settery
    void setState(double newState);
    void setSlope(double slopeValue);
    void setType(std::string typeValue);
    void setParam(double newState);
    void setCoeff(double coeffValue);


    //gettery
    double getSlope() const;
    double getState() const;
    double getCoeff() const;
    std::string getType() const;

private:
    double state = 0;
    double sloupe = 0;
    double coeff = 0;
    std::string type = "TREE";


};

#endif // CELL_H
