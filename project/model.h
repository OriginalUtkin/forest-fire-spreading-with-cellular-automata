#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <string>
#include <iostream>
#include <random>
#include <iomanip>
#include <limits>
#include <cmath>
#include <fstream>
#include <unistd.h>
#include <stdlib.h>

#include "cell.h"

const double sinValue = 0.707106781186548; // konstanta representujici velikost sinu

/**
 * @brief The model class - representuje simulacni model, ktery provadi celou simulaci.
 */
class model
{
    private:

        // Parametry potrebne pro vypocet a simulaci
        long int width; // sirka a vyska simulovane plochy
        long int treesCounter = 0; // celkovy pocet tree (je nutne pouze pro vypis vysledku)
        long int simTime;
        double windPower; // rychlost veetru
        double gradient;
        long int startX;
        long int startY;
        std::string forestType;
        std::string slopeDirection;
        std::string windDirection; // smer vetru
        std::vector<std::vector<long int>> objectCoordinates;

        // Metody vyuzite behem sumulaci
        std::vector<std::vector<cell>> getEnvirons(std::vector<std::vector<cell>>model, long int , long int);

        double applyRule(const std::vector<std::vector<cell>> n);
        double getWindCoefficient(long int number, std::string windDirection, double windStr);
        double getSlopeCoefficient(const double hk, const double h0);

        void setUnburnedPosition(std::vector<std::vector<cell>>&newPole, const long int xC, const long int yC, const long int horizontal, const long int vertical);


    public:
        model(long int w, std::string wd, double wp, long int sX, long int sY, std::vector<std::vector<long int>> oC, std::string fType, std::string slV, double grV); // Konstruktor
        ~model(); // Destructor

        std::vector<std::vector<cell>> pole;
        std::vector<std::vector<cell>> update(std::vector<std::vector<cell>> pole);

        long int getWidth();

        void printCurrentModel(const int timeStep, const std::vector<std::vector<cell>> arr);
        void simulationStart(); //zacina simulaci
        void printModel();
        void initModel();
        void debugPrint();
};
#endif // MODEL_H
