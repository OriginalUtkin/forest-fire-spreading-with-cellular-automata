#include "main.h"

int main(int argc, char** argv)
{
    errors errorString;
    int codeArguments = parseArguments(argc, argv);

    if(codeArguments != 0)
        printError();

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(600,600);
    glutCreateWindow("Forest fire spreading CA");
    setup();
    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}

void display(){

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // ocisteni obrazovky a buffru

    model simulator(_globalWidth, _globalWindDirection, _globalWindPower, _globalStartX, _globalStartY, _globalObject,  _globalType); // vytvoreni simulatru
    simulator.initModel(); // inicializace modelu simulatoru

    for(long int t = 0; t < _globalSimTime; ++t){

        GLfloat minSize = 60.0f/_globalWidth;
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0.0, 60, 60, 0.0, -1.0, 1.0);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glViewport(0, 0, 600, 600);

        for(int i = 0; i < simulator.getWidth(); ++i){

            for(int j = 0; j < simulator.getWidth(); ++j){

                if(((simulator.pole[i][j]).getType()).compare("UNBURNED") == 0){
                    glColor3f(0.0f, 0.0f, 0.0f); // cerna bunka

                }else{

                    if(((simulator.pole[i][j]).getState()) < 1){
                        glColor3f(0.0f, 0.8f, 0.0f);// zelena bunka

                    }else{
                        continue;
                    }
                }

                // Kresleni bunky ktera ma stav < 1 nebo ktera reprezenstuje objekt
                glBegin(GL_QUADS);
                glVertex2f(0.0f+minSize*j, 0.0f+minSize*i); //Left top
                glVertex2f(0.0f+minSize*(j+1), 0.0f+minSize*i); //Right top
                glVertex2f(0.0f+minSize*(j+1), 0.0f+minSize*(i+1)); //Right bottom
                glVertex2f(0.0f+minSize*j, 0.0f+minSize*(i+1)); //Left bottom
                glEnd();
            }

            for(int i = 0; i < simulator.getWidth(); i++) //for row in matrix
            {
                for(int j = 0; j < simulator.getWidth(); j++) //for col in matrix
                {

                    if((simulator.pole[i][j].getState()) == 1){

                        GLfloat xCenter = (0.0f+minSize*j + 0.0f+minSize*(j+1))/2 ;
                        GLfloat yCenter = (0.0f+minSize*i + 0.0f+minSize*(i+1))/2;

                        GLfloat radius = 0.6;//sqrt(x+y);
                        int triangleAmount = 20; //# of triangles used to draw circle
                        GLfloat twicePi = 2.0f * 3.14159265359;
                        glColor3f (1.0, 0.0, 0.0);

                        glBegin(GL_TRIANGLE_FAN);
                            glVertex2f(xCenter, yCenter); // center of circle
                            for(int o = 0; o <= triangleAmount;o++) {
                                glVertex2f(
                                        xCenter + (radius * cos(o *  twicePi / triangleAmount)),
                                    yCenter + (radius * sin(o * twicePi / triangleAmount))
                                );
                            }
                        glEnd();
                    }
                }
             }
        }

        glEnd();
        glutSwapBuffers();
        usleep(1);
        simulator.printCurrentModel(t, simulator.pole);
        simulator.pole = simulator.update(simulator.pole);
        std::cout<<  "Simulation time ->" << t << std::endl;
    }
}

void setup() {
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

int parseArguments(const int argCount,  char** argv){

    for(int counter = 1; counter < argCount; ++counter){
        std::string argument(argv[counter]);

        if(argument.compare("-w") == 0){

            std::cout << "-W argument" << std::endl;
            if((_globalWidth = checkArgumentValue(argv[counter + 1])) == -1)
                return 1;

        }else if(argument.compare("-t") == 0){

            std::cout << "-t argument" << std::endl;
            if((_globalSimTime = checkArgumentValue(argv[counter + 1])) == -1)
                return 1;

        }else if(argument.compare("-help") == 0){
            printHelp();

        }else if(argument.compare("-wd") == 0){

            std::cout << "-wd argument" << std::endl;
            if(!checkWindDirection(_globalWindDirection = argv[counter + 1]))
                return 1;

        }else if(argument.compare("-wp") == 0){

            std::cout << "-wp argument" << std::endl;
            if((_globalWindPower = checkWindPower(argv[counter + 1])) == -1)
                return 1;


        }else if(argument.compare("-start") == 0){

            std::cout << "-start argument" << std::endl;
            if((_globalStartX = checkPosition(argv[counter + 1])) == -1)
                return 1;

            if((_globalStartY = checkPosition(argv[counter + 2])) == -1)
                return 1;
        }else if(argument.compare("-o") == 0){

            std::vector<long int> objCoordinate;

            for(int i = 1 ; i < 5; ++i){
                \
                long int coordinate;

                if((coordinate = checkPosition(argv[counter + i])) == -1){
                    return 1;

                }else{
                    objCoordinate.push_back(coordinate);
                }
            }
            _globalObject.push_back(objCoordinate);


        }else if(argument.compare("-type") == 0){

            std::cout << "-type argument" << std::endl;

            _globalType = argv[counter + 1];

        }else if(argument.compare("-slope")){

            if(!checkSlope(_globalSlope = argv[counter + 1]))
                return 1;

        }else if(argument.compare("-gradient") == 0){


            if((_globalGradient = checkGradient(argv[counter + 1])) == -1)
                return 1;

        }else{
            continue;
        }
    }

    if(_globalStartX == -1 || _globalStartY == -1)
        return 1;


    return 0;
}


long int checkArgumentValue(std::string value){

    char* endPtr;
    long int testValue = strtol(value.c_str(), &endPtr, 10);

    if(*endPtr != '\0'){
        return -1;

    }else if(testValue <= 0){
        return -1;

    }else if(errno != 0){
        return -1;

    }else{
        return testValue;
    }
}

double checkWindPower(std::string value){

    char* endPtr;
    double testValue = std::strtod(value.c_str(), &endPtr);

    if(*endPtr != '\0'){
        return -1;

    }else if(testValue <= 0){
        return -1;

    }else if(errno != 0){
        return -1;

    }else{
        return testValue;
    }
}



bool checkSlope(std::string value){

    if (value.compare("W") == 0 ||
        value.compare("S") == 0 ||
        value.compare("E") == 0 ||
        value.compare("N") == 0 ||
        value.compare("none") == 0){
        return true;
    }else{
        return false;
    }
}

bool checkWindDirection(std::string value){
    if (value.compare("W") == 0 ||
        value.compare("S") == 0 ||
        value.compare("E") == 0 ||
        value.compare("N") == 0 ||
        value.compare("WN") == 0 ||
        value.compare("EN") == 0 ||
        value.compare("WS") == 0 ||
        value.compare("ES") == 0 ){
        return true;
    }else{
        return false;
    }
}

void printError(){

    std::cerr << _globalError << std::endl;
    exit(1);
}


long int checkPosition(std::string portString){

    char* p_end;
    long int portValue = strtol(portString.c_str(), &p_end, 10);

    if(portValue < 0 || portValue > _globalWidth)
        return -1;

    else if(errno == ERANGE)
        return -1;

    else if(*p_end != '\0')
        return -1;

    else
        return portValue;
}


long int checkGradient(std::string portString){

    char* p_end;
    long int portValue = strtol(portString.c_str(), &p_end, 10);

    if(portValue < 0)
        return -1;

    else if(errno == ERANGE)
        return -1;

    else if(*p_end != '\0')
        return -1;

    else
        return portValue;
}



void printHelp(){

    std::cout << helpMsg << std::endl;
    exit(0);
}


