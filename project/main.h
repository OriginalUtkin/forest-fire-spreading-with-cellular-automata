#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <unistd.h>


#include "model.h"

long int _globalWidth = 100;
long int _globalSimTime = 100;
long int _globalStartX = -1;
long int _globalStartY = -1;
double _globalWindPower = 1;
double _globalGradient = 0;
std::string _globalSlope = "";
std::string _globalType = "coniferous";
std::string _globalWindDirection = "none";
std::vector<std::vector<long int>> _globalObject;
std::string _globalError;


struct errors{
    std::string errArgument = "";

};

const std::string helpMsg = "";

int parseArguments(const int argCount,  char** argv);
long int checkArgumentValue(std::string value);
long int checkPosition(std::string portString);
long int checkGradient(std::string portString);
void printError();
void printHelp();
void setup();
void display();
double checkWindPower(std::string value);
bool checkWindDirection(std::string value);

