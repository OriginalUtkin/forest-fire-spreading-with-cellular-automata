#include "model.h"

/**
 * @brief Model::Model
 * @param w
 * @param h
 * @param t
 */
model::model(long int w, std::string wd, double wp, long int sX, long int sY, std::vector<std::vector<long int>> oC, std::string fType)
{
    width = w;
    windDirection = wd;
    windPower = wp;
    startX = sX;
    startY = sY;
    objectCoordinates = oC;
    forestType = fType;
}


/**
 * @brief Model::~Model
 */
model::~model(){}

/**
 * @brief Model::initModel
 */
void model::initModel(){

    cell modelCell;

    for(int countW = 0; countW < this->width; ++countW){
        std::vector<cell> row;
        for(int countH = 0; countH < this->width; ++countH){

            modelCell.setParam(0);

            if(this->forestType.compare("coniferous") == 0){
                modelCell.setCoeff(2.5);
            }else if(this->forestType.compare("broad-leaved") == 0){
                modelCell.setCoeff(1);
            }else{
                modelCell.setCoeff(1.5);
            }

            row.push_back(modelCell);
            this->treesCounter+=1;
        }
        this->pole.push_back(row);
    }

    this->pole[this->startX][this->startY].setState(1);

    // Inicializace objektu kteri ovlibnuji smer pozaru
    if(!objectCoordinates.empty()){
        for(unsigned int count = 0 ; count < objectCoordinates.size(); ++count)
            setUnburnedPosition(this->pole, objectCoordinates[count][0],objectCoordinates[count][1],objectCoordinates[count][2],objectCoordinates[count][3]);
    }
}


void model::printCurrentModel(const int timeStep, const std::vector<std::vector<cell>> arr){

    std::ofstream file;


    long int center = this->width / 2;
    long int allTrees = 0;

    for(int i =0 ; i < this->width; ++i){
        for(int j = 0; j < this->width; ++j){
            if((arr[i][j].getType()).compare("TREE") == 0)
                allTrees += 1;
        }
    }

    std::cout << allTrees << std::endl;

    long int east = 0;
    long int west = 0;
    long int north = 0;
    long int south = 0;

    for(long int i = 0; i < this->width; ++i){
        for(long int j = 0; j < this->width; ++j){

            if( j < center && arr[i][j].getState() == 1)
                west += 1;
            else if(j > center && arr[i][j].getState() == 1)
                east += 1;
            else
                continue;
        }
    }

    for(long int i = 0; i < this->width; ++i){
        for(long int j = 0; j < this->width; ++j){

            if( i < center && arr[i][j].getState() == 1)
                north += 1;
            else if(i > center && arr[i][j].getState() == 1)
                south += 1;
            else
                continue;
        }
    }

    double westPercent =  ((double) west / (double) allTrees) * (double)100 ;
    double eastPercent =  ((double) east /(double) allTrees) * (double)100;
    double southPercent = ((double) south / (double) allTrees) * (double)100;
    double northPercent =  ((double) north / (double) allTrees) * (double)100;

    file.open("CurrentState " + std::to_string(timeStep),std::ios::app);

    std::string statistic ="North : " + std::to_string(north) + "(" + std::to_string(northPercent)+"%)\n"+
                            "South : " + std::to_string(south)+ "(" + std::to_string(southPercent) +"%)\n"+
                            "West : " + std::to_string(west) + "(" + std::to_string(westPercent) +"%)\n"+
                            "East : " + std::to_string(east) + "(" + std::to_string(eastPercent) +"%)\n"+
            "\n________________________________________________________________\n\n";

    file << statistic;

    for(int i =0 ; i < this->width; ++i){
        for(int j = 0; j < this->width; ++j){

            std::string writeToFile =  "State cell [" + std::to_string(i) + "]" + "[" + std::to_string(j) +"]"+ "->" + std::to_string(arr[i][j].getState()) +
                    " Slope State : "+ std::to_string(arr[i][j].getSlope()) + "\n";
            file << writeToFile;
        }
    }

    file.close();
}


/**
 * @brief Model::getEnvirons
 * @param model
 * @param posW
 * @param posH
 * @return
 */
std::vector<std::vector<cell>> model::getEnvirons(const std::vector<std::vector<cell>> model, long int posW, long int posH){

    std::vector<std::vector<cell>> environs;
    cell nullCell; // neexistujici bunka

    for(long int w = posW - 1; w <= posW + 1; ++w){

        std::vector<cell> row;

        for(long int h = posH - 1; h <= posH + 1; ++h){
            try{ // pokus o pristup k bunce ktera muze byt mimo rozsah
                cell value = model.at(w).at(h);
                row.push_back(value);
            }
            catch(const std::out_of_range& e){ // bunka je mimo rozsah -> vloz nullCell
                row.push_back(nullCell);
            }
        }
        environs.push_back(row);
    }
    return environs;
}


double model::applyRule(const std::vector<std::vector<cell>> n){

    double currentState = n[1][1].getState();

    double N = n[0][1].getState() * getWindCoefficient(7, this->windDirection, this->windPower) * getSlopeCoefficient(n[0][1].getSlope(), n[1][1].getSlope());
    double W = n[1][0].getState() * getWindCoefficient(1, this->windDirection, this->windPower) * getSlopeCoefficient(n[1][0].getSlope(), n[1][1].getSlope());
    double E = n[1][2].getState() * getWindCoefficient(5, this->windDirection, this->windPower) * getSlopeCoefficient(n[1][2].getSlope(), n[1][1].getSlope());
    double S = n[2][1].getState() * getWindCoefficient(3, this->windDirection, this->windPower) * getSlopeCoefficient(n[2][1].getSlope(), n[1][1].getSlope());

    double NW = n[0][0].getState() * getWindCoefficient(8, this->windDirection, this->windPower) * getSlopeCoefficient(n[0][0].getSlope(), n[1][1].getSlope());
    double NE = n[0][2].getState() * getWindCoefficient(6, this->windDirection, this->windPower) * getSlopeCoefficient(n[0][2].getSlope(), n[1][1].getSlope());
    double SW = n[2][0].getState() * getWindCoefficient(2, this->windDirection, this->windPower) * getSlopeCoefficient(n[2][0].getSlope(), n[1][1].getSlope());
    double SE = n[2][2].getState() * getWindCoefficient(4, this->windDirection, this->windPower) * getSlopeCoefficient(n[2][2].getSlope(), n[1][1].getSlope());

    double newState = n[1][1].getCoeff() * (currentState + 0.4566 * (N + W + E + S) + 0.0788 * (NW + NE + SW + SE));

    return newState;
}


double model::getSlopeCoefficient(const double hk, const double h0){

    double finalCoefficient = pow(1.255, h0 - hk);

    return finalCoefficient;
}



double model::getWindCoefficient(long int number, std::string windDirection, double windStr){

    if(number == 1){

        if(windDirection.compare("N") == 0){
            return 1;

        }else if(windDirection.compare("S") == 0){
            return 1;

        }else if(windDirection.compare("W") == 0){
            return windStr;

        }else if(windDirection.compare("E") == 0){
            return 1;

        }else if(windDirection.compare("WN") == 0){
            return windStr * sinValue ;

        }else if(windDirection.compare("EN") == 0){
            return 1;

        }else if(windDirection.compare("WS") == 0){
            return windStr * sinValue;

        }else if(windDirection.compare("ES") == 0){
            return 1;
        }else{
            return 1;
        }

    }else if(number == 2){

        if(windDirection.compare("N") == 0){
            return 1;
        }else if(windDirection.compare("S") == 0){
            return windStr * sinValue ;
        }else if(windDirection.compare("W") == 0){
            return windStr * sinValue ;
        }else if(windDirection.compare("E") == 0){
            return 1;
        }else if(windDirection.compare("WN") == 0){
            return 1;
        }else if(windDirection.compare("EN") == 0){
            return 1;
        }else if(windDirection.compare("WS") == 0){
            return windStr;
        }else if(windDirection.compare("ES") == 0){
            return 1;
        }else{
            return 1;
        }

    }else if(number == 3){

        if(windDirection.compare("N") == 0){
            return 1;
        }else if(windDirection.compare("S") == 0){
            return windStr;
        }else if(windDirection.compare("W") == 0){
            return 1;
        }else if(windDirection.compare("E") == 0){
            return 1;
        }else if(windDirection.compare("WN") == 0){
            return 1;
        }else if(windDirection.compare("EN") == 0){
            return 1;
        }else if(windDirection.compare("WS") == 0){
            return windStr * sinValue ;
        }else if(windDirection.compare("ES") == 0){
            return windStr * sinValue ;
        }else{
            return 1;
        }

    }else if(number == 4){

        if(windDirection.compare("N") == 0){
            return 1;
        }else if(windDirection.compare("S") == 0){
            return windStr * sinValue ;
        }else if(windDirection.compare("W") == 0){
            return 1;
        }else if(windDirection.compare("E") == 0){
            return windStr * sinValue ;
        }else if(windDirection.compare("WN") == 0){
            return 1;
        }else if(windDirection.compare("EN") == 0){
            return 1;
        }else if(windDirection.compare("WS") == 0){
            return 1;
        }else if(windDirection.compare("ES") == 0){
            return windStr;
        }else{
            return 1;
        }

    }else if(number == 5){

        if(windDirection.compare("N") == 0){
            return 1;
        }else if(windDirection.compare("S") == 0){
            return 1;
        }else if(windDirection.compare("W") == 0){
            return 1;
        }else if(windDirection.compare("E") == 0){
            return windStr;
        }else if(windDirection.compare("WN") == 0){
            return 1;
        }else if(windDirection.compare("EN") == 0){
            return windStr * sinValue ;
        }else if(windDirection.compare("WS") == 0){
            return 1;
        }else if(windDirection.compare("ES") == 0){
            return windStr * sinValue;
        }else{
            return 1;
        }

    }else if(number == 6){

        if(windDirection.compare("N") == 0){
            return windStr * sinValue ;
        }else if(windDirection.compare("S") == 0){
            return 1;
        }else if(windDirection.compare("W") == 0){
            return 1;
        }else if(windDirection.compare("E") == 0){
            return windStr * sinValue ;
        }else if(windDirection.compare("WN") == 0){
            return 1;
        }else if(windDirection.compare("EN") == 0){
            return windStr;
        }else if(windDirection.compare("WS") == 0){
            return 1;
        }else if(windDirection.compare("ES") == 0){
            return 1;
        }else{
            return 1;
        }

    }else if(number == 7){

        if(windDirection.compare("N") == 0){
            return windStr;
        }else if(windDirection.compare("S") == 0){
            return 1;
        }else if(windDirection.compare("W") == 0){
            return 1;
        }else if(windDirection.compare("E") == 0){
            return 1;
        }else if(windDirection.compare("WN") == 0){
            return windStr * sinValue;
        }else if(windDirection.compare("EN") == 0){
            return windStr * sinValue;
        }else if(windDirection.compare("WS") == 0){
            return 1;
        }else if(windDirection.compare("ES") == 0){
            return 1;
        }else{
            return 1;
        }

    }else{
        if(windDirection.compare("N") == 0){
            return windStr * sinValue;
        }else if(windDirection.compare("S") == 0){
            return 1;
        }else if(windDirection.compare("W") == 0){
            return windStr * sinValue;
        }else if(windDirection.compare("E") == 0){
            return 1;
        }else if(windDirection.compare("WN") == 0){
            return windStr;
        }else if(windDirection.compare("EN") == 0){
            return 1;
        }else if(windDirection.compare("WS") == 0){
            return 1;
        }else if(windDirection.compare("ES") == 0){
            return 1;
        }else{
            return 1;
        }
    }
}


std::vector<std::vector<cell>> model::update(std::vector<std::vector<cell>> pole){

    // pole, reprezentujici dalsi krok simulaci
        std::vector<std::vector<cell>> nextStatePole;

        // inicializace pole pro dalsi krok
        cell modelCell;
        for(int countW = 0; countW < this->width; ++countW){
            std::vector<cell> row;
            for(int countH = 0; countH < this->width; ++countH){

                row.push_back(modelCell);
            }
            nextStatePole.push_back(row);
        }

        //  Vypocet stvau bunku v dalsim kroku. Vsichni parametry zustavaji stejne krome parametru state
        for(int i =0 ; i < this->width ; ++i){
            for(int j = 0; j < this->width; ++j){

                // Aktualni bunka je TREE a proto je nutne provest vypocet dalsiho stavu teto bunku
                if((pole[i][j].getType()).compare("TREE") == 0){

                    std::vector<std::vector<cell>> tmp = getEnvirons(pole,i,j);
                    double newState = applyRule(tmp);

                    if(newState > 1) // modle modelu je neni potreba vypocitat stav vice nez 1
                        newState = 1;

                    nextStatePole[i][j].setState(newState);
                    nextStatePole[i][j].setCoeff(pole[i][j].getCoeff());
                    nextStatePole[i][j].setSlope(pole[i][j].getSlope());
                    nextStatePole[i][j].setType(pole[i][j].getType());
                }

                // Jinak, bunka je neni TREE a nemuze svuj stav zmenit -> je nutne nechat soucasny stav bunky avsichni ostatni par.
                else{
                    nextStatePole[i][j].setState(0);
                    nextStatePole[i][j].setCoeff(0);
                    nextStatePole[i][j].setSlope(pole[i][j].getSlope());
                    nextStatePole[i][j].setType(pole[i][j].getType());
                }
            }
        }

        return nextStatePole;
}



void model::setUnburnedPosition(std::vector<std::vector<cell>>&newPole, const long int xC, const long int yC, const long int horizontal, const long int vertical){

    newPole[xC][yC].setType("UNBURNED");

    for(long int i = xC - horizontal; i <= xC + horizontal; ++i){
        for(long int j = yC - vertical; j <= yC + vertical; ++j){
            newPole[i][j].setType("UNBURNED");
        }
    }
}




/**
 * @brief Model::getWidth
 * @return
 */
long int model::getWidth(){
    return this->width;
}




/**
 * @brief Model::printModel debagovaci vypis simulacniho modelu
 */
void model::printModel(){


}







